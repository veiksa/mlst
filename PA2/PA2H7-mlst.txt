*WARNING*: No perfect match for ACS. ACS_139 is the closest match - contig name: PA2H7_c5	allele length: 390	HSP length: 302	Gaps: 2	Percent ID:  80.13
MLST allele seq: ggcccgctggccaacggcgcgaccaccgtgctgttcgaaggcgtgccgaactacccggac
                    ||||||||||||||||||||| |  || |||||||||||||||||||||||||||||
Hit in genome:   cgggccgctggccaacggcgcgacctcgctgatgttcgaaggcgtgccgaactacccgga

MLST allele seq: atcacccgcgtcgccaggatcgtcgacaagcacaaggtcaacatcctctacaccgcgccc
                 | | | ||  ||   |   || |||||||||||||||| | |||| |||||||||| || 
Hit in genome:   cacctcacgtttctggaacgtcatcgacaagcacaaggtgaccatcttctacaccgcccc

MLST allele seq: accgccatccgcgccatgatggccgagggcaaggccgcggtcgaaggcgccgacg-gctc
                 ||||||||||| ||  |||||  ||| ||| |||   |||| ||||  | || || || |
Hit in genome:   gaccgccatccgtgcgctgatgcgcgaaggcgaggagccgg-tgaagaagacgtcgcgca

MLST allele seq: cagcctgcgcctgctgggctcggtgggcgagccgatcaacccggaagcctggcagtggta
                     ||||||||||| |||   || |||||||||||||| |||||||||||||  |||||
Hit in genome:   cgtcgctgcgcctgctcggcagcgtcggcgagccgatcaatccggaagcctggcgctggt

MLST allele seq: ctacgagaccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagac
                 ||||||    |||||| |    ||||||||||||||||| ||||||||||||||||||||
Hit in genome:   actacgaagtggtcggcgacagccgctgcccgatcgtcgatacctggtggcagaccgaga

MLST allele seq: cggcgcctgcctgatgaccccgctgccgggcgcccacgcgctgaagccgggctccgccgc
                 |||||
Hit in genome:   ccggcggcatcctgatctcgccgctggccggcgcgatggacctcaagccggggtcggcca

MLST allele seq: caagccgttcttcggcgtggtgccggccctg
                 
Hit in genome:   ccctgcccttcttcggcgtgcagccggcgct

*WARNING*: No perfect match for ARO. ARO_110 is the closest match - contig name: PA2H7_c117	allele length: 498	HSP length: 42	Gaps: 0	Percent ID:  85.71
MLST allele seq: acgtcaccgtgcccttcaaggaagacgcctaccgcctgtgcggtgagctcaccgagcgcg
                                                                             
Hit in genome:   ccaacgtcaccctgccgcacaaggaagccgcgttctcggtgtgcaccacgctgaccgcgc

MLST allele seq: cccgccgcgccggcgcggtgaacaccctgaagaggctcgacgacggccgcctgctgggcg
                                                                             
Hit in genome:   gcgcgcgccgcgccggctcggtcaacacgctgctgcgcaagggcgaccgctggcacggcg

MLST allele seq: acaacaccgatggcgccgggctgacccgcgacctggtggtgaacaacggcgtggcgctca
                                                                             
Hit in genome:   acaccaccgatggcatcggcctggtgcgcgacctgaccgaccgccacggcctggacctgc

MLST allele seq: agggccgccgcgtgctggtgctgggcgccggcggcgcggtgcgcggcatcctcgaaccct
                   ||||||||||||||| || | |||||||| ||| ||| ||||
Hit in genome:   gcggccgccgcgtgctgatgatcggcgccggtggctcggcgcgcagcgtcgccccggcgc

MLST allele seq: tcctggccgagggcccggcggcgctggtggtgctcaaccgcaccgcggccaaggccgagc
                 
Hit in genome:   tgctcgatgccggcatcaccgaactggtggtggtcaaccgcacgccggagcgcgccgacg

MLST allele seq: agctggcccgcgagttcgccgacctgggcccggtgcgcgccggcggcttcgacctgcgcg
                 
Hit in genome:   aactgatcgacgcgatgggcgagcccggccgtgcgatcagccgctactgggaagacctgc

MLST allele seq: acgaacccttcgacctgatcgtcaacggcacctcggccagcctggccggcgagctgccgc
                 
Hit in genome:   gcgacctcggcgacttcgaactgatcgtcaatgccacctcggctggccgcgaccgcgacg

MLST allele seq: ccatcgacgcggcgctgatccgcccggggcacaccgtctgctacgacatgatgtacggca
                 
Hit in genome:   tcgagttcaagctgccgctgtcgctggtcaattcgatgaccactgctgtcgacctgaact

MLST allele seq: aggaaccgactgccttca
                 
Hit in genome:   acggcgaggcggccatcg

*WARNING*: No perfect match for GUA. GUA_121 is the closest match - contig name: PA2H7_c442	allele length: 373	HSP length: 373	Gaps: 0	Percent ID:  97.32
MLST allele seq: atcctgggcctgtccggcggcgtcgattcgtccgttgtggccgcgctgctgcacaaggcc
                 ||||||||||||||||| ||||||||||||||||| ||||||||||||||||||||||||
Hit in genome:   atcctgggcctgtccggtggcgtcgattcgtccgtagtggccgcgctgctgcacaaggcc

MLST allele seq: atcggcgagaagctgacctgcgtgtttgtggataccggcctgctgcgctggcaggaaggc
                 |||||||||||||||||||||||||| |||||||||||||||||||| ||||||||||||
Hit in genome:   atcggcgagaagctgacctgcgtgttcgtggataccggcctgctgcgttggcaggaaggc

MLST allele seq: gaccaggtgatggcgatgttcgccgagcacatgggcgtgaaggtcgttcgcgtgaacgcc
                 |||||||||||||||||||||||||||||||||||||| |||||||||||||||||||||
Hit in genome:   gaccaggtgatggcgatgttcgccgagcacatgggcgtaaaggtcgttcgcgtgaacgcc

MLST allele seq: gccgaccgttacttcgccgcgctggaaggcgtgagcgacccggaagccaagcgcaagatc
                 || ||||||||||| || ||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gcagaccgttactttgctgcgctggaaggcgtgagcgacccggaagccaagcgcaagatc

MLST allele seq: atcggcaacctgttcgttgagatcttcgacgaagagtcgaacaagctgaagaacgccaag
                 ||||||||||||||||| ||||||||||||||||| ||||||||||||||||||||||||
Hit in genome:   atcggcaacctgttcgtcgagatcttcgacgaagaatcgaacaagctgaagaacgccaag

MLST allele seq: tggctggcgcagggcaccatctacccggacgtgatcgagtcggccggcagcaagaccggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tggctggcgcagggcaccatctacccggacgtgatcgagtcggccggcagcaagaccggc

MLST allele seq: aaggcgcatgtga
                 |||||||||||||
Hit in genome:   aaggcgcatgtga

*WARNING*: No perfect match for MUT. MUT_105 is the closest match - contig name: PA2H7_c279	allele length: 442	HSP length: 120	Gaps: 0	Percent ID:  84.17
MLST allele seq: ttgcaggaagtcatcaagcgcctggccctggcccgcttcgacgtggccttccacctgcgc
                                                                             
Hit in genome:   ggccatatcgaagagtggctgcgttcgctggcgctggcacggccggatgtcgagctgcgc

MLST allele seq: cacaacggcaagaccatcttcagcctgcacgaggccgcggacgaggtcgcccgcgcgcgc
                                                                             
Hit in genome:   gtgtcgcacaacggcaaggcctcgcgtcgttacaagccgggcgatctgtattccgacaca

MLST allele seq: cgcgtcggcgccgtgtgcggcccggccttcctcgaacaggcgctgcccatcgagatcgag
                                                                             
Hit in genome:   cgccttgccgaaacgctgggcgaggatttcgccaaccaggccgtgcgcgtggaccacagc

MLST allele seq: cgcaacggcctgcacctgtggggctgggtcggcctgccgaccttctcccgcagccagccg
                                                                             
Hit in genome:   ggtgccggactgcgcctgcatggctggatcgcgcagccgcattattcgcgcgcgagcgcc

MLST allele seq: gacctgcagtacttctacgtcaacggccgcatggtgcgcgacaagctggtcgcccacgcg
                                                                             
Hit in genome:   gaccagcagtacctgtacgtcaacggtcgctcggtgcgtgaccgcagcgtcgcccacgca

MLST allele seq: gtgcgccaggcctaccgcgacgtgctctacaacggccgccacccgaccttcgtgctgttc
                         ||||||| |||| ||||| ||| |||||||||| ||| |||  |||||||||
Hit in genome:   gtgaagatggcctacggcgatgtgctgtaccacggccgccagccggcctatgtgctgttc

MLST allele seq: ctggaggtcgatccggcgacggtggacgtcaacgtgcacccgaccaagcacgaagtgcgc
                 |||||  | |||||| |    || |||||||||||||||||  |||||||||||||||||
Hit in genome:   ctggaactggatccgacccgcgtcgacgtcaacgtgcaccctgccaagcacgaagtgcgc

MLST allele seq: ttccgtgacagccgcatggtcc
                 ||||||||
Hit in genome:   ttccgtgattcgcggctggtcc

*WARNING*: No perfect match for NUO. NUO_47 is the closest match - contig name: PA2H7_c485	allele length: 366	HSP length: 62	Gaps: 0	Percent ID:  80.65
MLST allele seq: atgttcctcaacctcggcccgaaccacccgtccgcccacggtgcgttccgcatcatcctg
                              |||||||| | || ||| |||| ||||||| | | ||| | ||||||
Hit in genome:   tacaccatgaacttcggcccgcagcatccggccgctcacggtgtgctgcgcctgatcctg

MLST allele seq: caactggacggcgaagagatcatcgactgcgtcccggagatcggctaccaccatcgcggc
                  || |||||||||||
Hit in genome:   gaaatggacggcgaaaccatcatgcgtgccgacccgcacgtgggtctgctgcaccgtggc

MLST allele seq: gccgagaagatggccgagcgccagtcctggcacagtttcatcccctacaccgaccgcatc
                 
Hit in genome:   accgagaagctggccgagtccaagccgttcaaccagtcgatcggctacatggatcgcctc

MLST allele seq: gactacctcggcggggtgatgaacaacctgccctacgtactttcggtggagaagctcgcc
                 
Hit in genome:   gactacgtgtcgatgatgtgcaacgagcacgcctacgtgcgcgcgatcgagaccctgatg

MLST allele seq: ggaatcaaggtgccccagcgggtcgacgtgatccggatcatgatggcggagttcttccgt
                 
Hit in genome:   ggcatcgaggcgccggagcgtgcgcagtacatccgcaccatgtacgacgagatcacccgc

MLST allele seq: atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg
                 
Hit in genome:   atcctcaaccacctgatgtggctgggctccaacgcgctcgacctgggtgcgatggccgtg

MLST allele seq: gtgttc
                 
Hit in genome:   atgctg

*WARNING*: No perfect match for PPS. PPS_100 is the closest match - contig name: PA2H7_c338	allele length: 370	HSP length: 170	Gaps: 0	Percent ID:  90.59
MLST allele seq: tatcgttcaggcccgccccgagaccgtgaagagccgtgccagcgccaccgtcatggagcg
                                                                             
Hit in genome:   cgtgcaggcgcgcccggaaacggtgaagtcgcgcagccacgccacccagatcgagcgctt

MLST allele seq: ctacctgctgaaagagaaggggaccgtcctggtcgagggtcgcgccatcggccagcgtat
                                                                             
Hit in genome:   cgcgctgaccgagaagggcggcaacgtgctggccgaaggccgcgccgtcggcgccaagat

MLST allele seq: cggcgccggcccggtcaaggtgatcaacgacgtgtcggaaatggacaaggtccagccggg
                                                                     ||||||||
Hit in genome:   cggctcgggcgtggcccgcgtggtgaagacgctggacgacatgaaccgcgtgcagccggg

MLST allele seq: cgacgtcctggtctccgacatgaccgatccggactgggagccggtgatgaagcgcgctag
                 |||||| ||| || |||||||||||||||| || ||||| ||||||||||||||||||  
Hit in genome:   cgacgtactgatcgccgacatgaccgatcccgattgggaaccggtgatgaagcgcgcttc

MLST allele seq: cgccatcgtcaccaaccgtggcgggcgtacctgccacgccgcgatcatcgcccgcgagct
                  ||||||||||||||||||||||| |||||||||||||||||||||||||| ||||||||
Hit in genome:   ggccatcgtcaccaaccgtggcggccgtacctgccacgccgcgatcatcgcgcgcgagct

MLST allele seq: gggcattccggccgtggtcggttgtggcaacgccacccaggtcctgcaggacggccaggg
                 |||| | ||||||||||||||||  |||||||||||| ||||
Hit in genome:   gggcgtgccggccgtggtcggttcgggcaacgccaccaaggtgatcgaagatggccagct

MLST allele seq: cgtgacggtt
                 
Hit in genome:   ggtcaccgtc

*WARNING*: No perfect match for TRP. TRP_104 is the closest match - contig name: PA2H7_c641	allele length: 443	HSP length: 80	Gaps: 0	Percent ID:  87.50
MLST allele seq: cgtggtgggcagctcgccggaagtgctggtgcgcgtggaggacggcgaggtcacggtgcg
                              ||||||||| | ||||| ||| |  |||| ||||| ||||| |||||
Hit in genome:   ggtggtcggttcatcgccggaaatcctggtacgccttcaggatggcgaagtcaccgtgcg

MLST allele seq: cccgatcgccggcacccgcccgcgcggcgccaccgaggaagccgaccgcgccctggagca
                 |||||||||||||||||||||||| ||||||||
Hit in genome:   cccgatcgccggcacccgcccgcgtggcgccacgcccgagctggacctggcgctggaagc

MLST allele seq: ggacctgctgtccgacgccaaggaaatcgccgagcacctgatgctcatcgacctgggccg
                 
Hit in genome:   cgagctgctggcagatccgaaggaacgcgccgagcacctgatgctgatcgacctcggccg

MLST allele seq: caacgacgtcggccgcgtgtcgcagaccggttcggtgaaggtcaccgagcagatggtcat
                 
Hit in genome:   caacgatgccggccgcgtctcgaaggcaggtaccgtggaggtgggcgagcagttcgtgat

MLST allele seq: cgagcgctactccaacgtcatgcacatcgtttccaacgtcaacggccagctcaagcccga
                 
Hit in genome:   cgaacgctacagccacgtgatgcacatcgtcagcgaagtgaccgggcagctgcagccggg

MLST allele seq: gctcagcgccatggacgccctgcgcgcgatcctgccggccggcaccctgtccggcgcgcc
                 
Hit in genome:   cctgagctatgccgacgtgctgcgtgccaccttccctgccggcacggtcagcggcgcgcc

MLST allele seq: gaagatccgcgccatggaaatcatcgacgagctggagccggtcaagcgcggcgtctacgg
                 
Hit in genome:   gaagatccgcgcgctggaagtgatccgcgagctggaaccgatcaagcgcaatgtctacgc

MLST allele seq: cggcgccgtgggctacctggcgt
                 
Hit in genome:   cggcagcatcggctacatcggct

*WARNING*: No perfect match for .  is the closest match - contig name: 	allele length: 	HSP length: 	Gaps: 	Percent ID:   0.00
Unknown ST	ACS_139	ARO_110	GUA_121	MUT_105	NUO_47	PPS_100	TRP_104	
