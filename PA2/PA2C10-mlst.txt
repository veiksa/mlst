ACS_41 is a perfect match - contig name: PA2C10_c31	allele length: 390	HSP length: 390	Gaps: 0	Percent ID: 100.00
MLST allele seq: ggcccgctggccaacggcgccaccaccattctgttcgagggcgtgccgaactaccccgac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcccgctggccaacggcgccaccaccattctgttcgagggcgtgccgaactaccccgac

MLST allele seq: gtgacccgcgtggcgaagatcatcgacaagcacaaggtcaacatcctctacaccgcgccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgacccgcgtggcgaagatcatcgacaagcacaaggtcaacatcctctacaccgcgccg

MLST allele seq: accgcgatccgcgcgatgatggccgaaggcaaggcggcggtggccggtgccgacggttcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   accgcgatccgcgcgatgatggccgaaggcaaggcggcggtggccggtgccgacggttcc

MLST allele seq: agcctgcgtctgctcggttcggtgggcgagccgatcaacccggaagcctggcagtggtac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   agcctgcgtctgctcggttcggtgggcgagccgatcaacccggaagcctggcagtggtac

MLST allele seq: tacgagaccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tacgagaccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc

MLST allele seq: ggcgcctgcctgatgaccccgctgccgggcgcccacgcgatgaagccgggctccgcggcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcgcctgcctgatgaccccgctgccgggcgcccacgcgatgaagccgggctccgcggcc

MLST allele seq: aagccgttcttcggcgtggtaccggcactg
                 ||||||||||||||||||||||||||||||
Hit in genome:   aagccgttcttcggcgtggtaccggcactg

ARO_55 is a perfect match - contig name: PA2C10_c780	allele length: 498	HSP length: 498	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgtcaccgtgccgttcaaggaagaggcctatcgtctggtggacgagttgagcgagcggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atgtcaccgtgccgttcaaggaagaggcctatcgtctggtggacgagttgagcgagcggg

MLST allele seq: ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg

MLST allele seq: acaacaccgacggcgccggcctgctgcgggacctgacggcgaacgccggggtcgagctgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acaacaccgacggcgccggcctgctgcgggacctgacggcgaacgccggggtcgagctgc

MLST allele seq: gcggcaagcgggttctcctgctcggcgccggcggtgcggtgcgtggggtgctcgaaccct
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gcggcaagcgggttctcctgctcggcgccggcggtgcggtgcgtggggtgctcgaaccct

MLST allele seq: tcctcggcgagtgcccggcggaggtgctgatcgccaaccgcacggcgcggaaggccgtgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tcctcggcgagtgcccggcggaggtgctgatcgccaaccgcacggcgcggaaggccgtgg

MLST allele seq: acctggccgagcggttcgccgacctcggcgcggtgcacggctgcggtttcgccgaggtcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acctggccgagcggttcgccgacctcggcgcggtgcacggctgcggtttcgccgaggtcg

MLST allele seq: aagggcctttcgacctgatcgtcaacggcacctcggccagtcttgccggcgacgtgccgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   aagggcctttcgacctgatcgtcaacggcacctcggccagtcttgccggcgacgtgccgc

MLST allele seq: cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatgcca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatgcca

MLST allele seq: aggaaccgactgccttca
                 ||||||||||||||||||
Hit in genome:   aggaaccgactgccttca

GUA_7 is a perfect match - contig name: PA2C10_c77	allele length: 373	HSP length: 373	Gaps: 0	Percent ID: 100.00
MLST allele seq: ctgctcggcctctccggcggcgtggactcctcggtggtcgccgcgctgctgcacaaggcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctgctcggcctctccggcggcgtggactcctcggtggtcgccgcgctgctgcacaaggcc

MLST allele seq: atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc

MLST allele seq: gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc

MLST allele seq: gaggacaagttcctcggccgcctggccggcgtcgccgacccggaagagaagcgcaagatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaggacaagttcctcggccgcctggccggcgtcgccgacccggaagagaagcgcaagatc

MLST allele seq: atcggccgcaccttcatcgaagttttcgacgaagaagccaccaagctgcaggacgtgaag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggccgcaccttcatcgaagttttcgacgaagaagccaccaagctgcaggacgtgaag

MLST allele seq: ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc

MLST allele seq: aaggcccacgtga
                 |||||||||||||
Hit in genome:   aaggcccacgtga

MUT_61 is a perfect match - contig name: PA2C10_c599	allele length: 442	HSP length: 442	Gaps: 0	Percent ID: 100.00
MLST allele seq: ctgcaggaggtcatcaagcgcctggcgctggcccgcttcgacgtggctttccacctgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctgcaggaggtcatcaagcgcctggcgctggcccgcttcgacgtggctttccacctgcgc

MLST allele seq: cacaacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cacaacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc

MLST allele seq: cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag

MLST allele seq: cgcaacggcctgcacctgtggggctgggtcggcttgccgaccttctcccgcagccagccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgcaacggcctgcacctgtggggctgggtcggcttgccgaccttctcccgcagccagccg

MLST allele seq: gacctgcagtacttctatgtgaacgggcgcatggtgcgcgacaagctggtcgcccacgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gacctgcagtacttctatgtgaacgggcgcatggtgcgcgacaagctggtcgcccacgcg

MLST allele seq: gtgcgccaggcttatcgcgacgtgctgtacaacggccggcatccgacctttgtgctgttc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgcgccaggcttatcgcgacgtgctgtacaacggccggcatccgacctttgtgctgttc

MLST allele seq: ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc

MLST allele seq: ttccgtgacagccggatggtcc
                 ||||||||||||||||||||||
Hit in genome:   ttccgtgacagccggatggtcc

NUO_2 is a perfect match - contig name: PA2C10_c69	allele length: 366	HSP length: 366	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg

MLST allele seq: caactggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   caactggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc

MLST allele seq: gccgagaagatggccgagcgccagtcctggcacagtttcattccctacaccgaccgcatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gccgagaagatggccgagcgccagtcctggcacagtttcattccctacaccgaccgcatc

MLST allele seq: gactacctcggcggggtgatgaacaacctgccctacgtactctcggtggagaagctcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gactacctcggcggggtgatgaacaacctgccctacgtactctcggtggagaagctcgcc

MLST allele seq: gggatcaaggtgccgcagcgggtcgacgtgatccggatcatgatggcggagttcttccgt
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggatcaaggtgccgcagcgggtcgacgtgatccggatcatgatggcggagttcttccgt

MLST allele seq: atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg

MLST allele seq: gtgttc
                 ||||||
Hit in genome:   gtgttc

PPS_29 is a perfect match - contig name: PA2C10_c24	allele length: 370	HSP length: 370	Gaps: 0	Percent ID: 100.00
MLST allele seq: catcgttcaggcacgcccggaaaccgtgaagagccgcgccagtgccacggtcatggagcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   catcgttcaggcacgcccggaaaccgtgaagagccgcgccagtgccacggtcatggagcg

MLST allele seq: ctacctgctgaaagagaaggggaccgtcctggtggaaggacgtgccatcggccagcgcat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctacctgctgaaagagaaggggaccgtcctggtggaaggacgtgccatcggccagcgcat

MLST allele seq: cggtgccggtccggtcaaggtgatcaacgacgtgtcggaaatggacaaggtccaaccggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cggtgccggtccggtcaaggtgatcaacgacgtgtcggaaatggacaaggtccaaccggg

MLST allele seq: tgacgtcctggtctccgacatgaccgacccggactgggagccggtgatgaagcgcgccag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgacgtcctggtctccgacatgaccgacccggactgggagccggtgatgaagcgcgccag

MLST allele seq: cgccatcgtcaccaaccgcggcgggcgtacctgccacgcggcgatcatcgctcgcgaact
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgccatcgtcaccaaccgcggcgggcgtacctgccacgcggcgatcatcgctcgcgaact

MLST allele seq: gggcatcccggcggtggtcggttgcggcaacgccacccagatcctgcaggatggccaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggcatcccggcggtggtcggttgcggcaacgccacccagatcctgcaggatggccaggg

MLST allele seq: ggtgaccgtt
                 ||||||||||
Hit in genome:   ggtgaccgtt

TRP_7 is a perfect match - contig name: PA2C10_c448	allele length: 443	HSP length: 443	Gaps: 0	Percent ID: 100.00
MLST allele seq: tgtcgtgggcagctcgccggaggtgctggtacgggtcgaggatggcctggtgacggtgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgtcgtgggcagctcgccggaggtgctggtacgggtcgaggatggcctggtgacggtgcg

MLST allele seq: cccgatcgccggtacccgtccgcgcgggatcaacgaagaggccgacctggcactggagca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cccgatcgccggtacccgtccgcgcgggatcaacgaagaggccgacctggcactggagca

MLST allele seq: ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg

MLST allele seq: caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat

MLST allele seq: cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg

MLST allele seq: gctcagcgcgatggacgcgctgcgggcgattctgccggcgggcactctatccggcgcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gctcagcgcgatggacgcgctgcgggcgattctgccggcgggcactctatccggcgcgcc

MLST allele seq: gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggagtctacgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggagtctacgg

MLST allele seq: cggcgcggtcggctacctggcat
                 |||||||||||||||||||||||
Hit in genome:   cggcgcggtcggctacctggcat

*WARNING*: No perfect match for .  is the closest match - contig name: 	allele length: 	HSP length: 	Gaps: 	Percent ID:   0.00
Unknown ST	ACS_41	ARO_55	GUA_7	MUT_61	NUO_2	PPS_29	TRP_7	
