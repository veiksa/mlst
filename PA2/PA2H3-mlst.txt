ACS_14 is a perfect match - contig name: PA2H3_c804	allele length: 390	HSP length: 390	Gaps: 0	Percent ID: 100.00
MLST allele seq: ggcccgttggccaacggcgccaccaccattctgttcgagggcgtgccgaactaccccgac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcccgttggccaacggcgccaccaccattctgttcgagggcgtgccgaactaccccgac

MLST allele seq: gtgacccgcgtggcgaaaatcatcgacaagcacaaggtcaacatcctctacaccgcgccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgacccgcgtggcgaaaatcatcgacaagcacaaggtcaacatcctctacaccgcgccg

MLST allele seq: accgcgatccgcgcgatgatggctgaaggcaaggcggcggtggccggtgccgacggttcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   accgcgatccgcgcgatgatggctgaaggcaaggcggcggtggccggtgccgacggttcc

MLST allele seq: agcctgcgtctgctcggttcggtgggcgagccgatcaacccggaagcgtggcagtggtac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   agcctgcgtctgctcggttcggtgggcgagccgatcaacccggaagcgtggcagtggtac

MLST allele seq: tacgaggccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tacgaggccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc

MLST allele seq: ggcgcctgcctgatgaccccgttgccgggcgcccatgcgatgaagccgggctccgcggcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcgcctgcctgatgaccccgttgccgggcgcccatgcgatgaagccgggctccgcggcc

MLST allele seq: aagccgttcttcggcgtggtcccggcgctg
                 ||||||||||||||||||||||||||||||
Hit in genome:   aagccgttcttcggcgtggtcccggcgctg

ARO_5 is a perfect match - contig name: PA2H3_c164	allele length: 498	HSP length: 498	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgtcaccgtgccgttcaaggaagaggcctatcgtctggtggacgagttgagcgagcggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atgtcaccgtgccgttcaaggaagaggcctatcgtctggtggacgagttgagcgagcggg

MLST allele seq: ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg

MLST allele seq: acaacaccgacggcgccggcctgctgcgggacctgacggcgaacgccggggtcgagctgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acaacaccgacggcgccggcctgctgcgggacctgacggcgaacgccggggtcgagctgc

MLST allele seq: gcggcaagcgggttctcctgctcggcgccggcggtgcggtgcgtggggtgctcgaaccct
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gcggcaagcgggttctcctgctcggcgccggcggtgcggtgcgtggggtgctcgaaccct

MLST allele seq: tcctcggcgagtgcccggcggagttgctgatcgccaaccgcacggcgcggaaggccgtgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tcctcggcgagtgcccggcggagttgctgatcgccaaccgcacggcgcggaaggccgtgg

MLST allele seq: acctggccgagcggttcgccgacctcggcgcggtgcacggctgcggtttcgccgaggtcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acctggccgagcggttcgccgacctcggcgcggtgcacggctgcggtttcgccgaggtcg

MLST allele seq: aagggcctttcgacctgatcgtcaacggcacctcggccagtcttgccggcgacgtgccgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   aagggcctttcgacctgatcgtcaacggcacctcggccagtcttgccggcgacgtgccgc

MLST allele seq: cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatgcca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatgcca

MLST allele seq: aggaaccgactgccttca
                 ||||||||||||||||||
Hit in genome:   aggaaccgactgccttca

GUA_10 is a perfect match - contig name: PA2H3_c130	allele length: 373	HSP length: 373	Gaps: 0	Percent ID: 100.00
MLST allele seq: ctgctaggcctctccggcggcgtggactcctcggtggtcgccgcgctgctgcacaaggcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctgctaggcctctccggcggcgtggactcctcggtggtcgccgcgctgctgcacaaggcc

MLST allele seq: atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc

MLST allele seq: gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc

MLST allele seq: gaggacaagttcctcggccgcctggccggcgtcgccgatccggaagagaagcgcaagatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaggacaagttcctcggccgcctggccggcgtcgccgatccggaagagaagcgcaagatc

MLST allele seq: atcggccgcaccttcatcgaagtcttcgacgaagaagccaccaagctgcaggacgtgaag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggccgcaccttcatcgaagtcttcgacgaagaagccaccaagctgcaggacgtgaag

MLST allele seq: ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc

MLST allele seq: aaggcccacgtga
                 |||||||||||||
Hit in genome:   aaggcccacgtga

MUT_7 is a perfect match - contig name: PA2H3_c254	allele length: 442	HSP length: 442	Gaps: 0	Percent ID: 100.00
MLST allele seq: ctgcaggaagtcatcaagcgcctggcgctggcccgtttcgacgtggctttccacctgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctgcaggaagtcatcaagcgcctggcgctggcccgtttcgacgtggctttccacctgcgc

MLST allele seq: cacaacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cacaacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc

MLST allele seq: cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag

MLST allele seq: cgcaacggcctgcacctgtggggttgggtcggcttgccgaccttctctcgcagccagccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgcaacggcctgcacctgtggggttgggtcggcttgccgaccttctctcgcagccagccg

MLST allele seq: gacctgcagtacttctatgtgaacgggcgcatggtgcgcgacaagctggtcgcccacgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gacctgcagtacttctatgtgaacgggcgcatggtgcgcgacaagctggtcgcccacgcg

MLST allele seq: gtgcgccaggcttatcgcgacgtgctgtacaacggccggcatccgaccttcgtgctgttc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgcgccaggcttatcgcgacgtgctgtacaacggccggcatccgaccttcgtgctgttc

MLST allele seq: ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc

MLST allele seq: ttccgtgacagccggatggtcc
                 ||||||||||||||||||||||
Hit in genome:   ttccgtgacagccggatggtcc

NUO_4 is a perfect match - contig name: PA2H3_c517	allele length: 366	HSP length: 366	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg

MLST allele seq: caactggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   caactggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc

MLST allele seq: gccgagaagatggccgagcgccagtcctggcacagtttcatcccctacaccgaccgcatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gccgagaagatggccgagcgccagtcctggcacagtttcatcccctacaccgaccgcatc

MLST allele seq: gactacctcggcggggtgatgaacaacctgccctacgtactctcggtggagaagctcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gactacctcggcggggtgatgaacaacctgccctacgtactctcggtggagaagctcgcc

MLST allele seq: gggatcaaggtgccccagcgggtcgacgtgatccggatcatgatggcggagttcttccgt
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggatcaaggtgccccagcgggtcgacgtgatccggatcatgatggcggagttcttccgt

MLST allele seq: atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg

MLST allele seq: gtgttc
                 ||||||
Hit in genome:   gtgttc

PPS_13 is a perfect match - contig name: PA2H3_c405	allele length: 370	HSP length: 370	Gaps: 0	Percent ID: 100.00
MLST allele seq: catcgtccaggcacgcccggaaaccgtgaagagccgcgccagcgccacggtcatggagcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   catcgtccaggcacgcccggaaaccgtgaagagccgcgccagcgccacggtcatggagcg

MLST allele seq: ctacctgctgaaagagaaggggaccgtcctggtggaagggcgtgccatcggccagcgcat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctacctgctgaaagagaaggggaccgtcctggtggaagggcgtgccatcggccagcgcat

MLST allele seq: cggtgccggtccggtcaaagtgatcaacgacgtgtcggaaatggacaaggtccaaccggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cggtgccggtccggtcaaagtgatcaacgacgtgtcggaaatggacaaggtccaaccggg

MLST allele seq: tgacgtcctggtctccgacatgaccgacccggactgggagccagtgatgaagcgcgccag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgacgtcctggtctccgacatgaccgacccggactgggagccagtgatgaagcgcgccag

MLST allele seq: cgccatcgtcaccaaccgcggcgggcgcacctgccacgcggcgatcatcgctcgcgaact
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgccatcgtcaccaaccgcggcgggcgcacctgccacgcggcgatcatcgctcgcgaact

MLST allele seq: gggcatcccggcggtggtcggttgcggcaacgccacccagatcctgcaggatggccaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggcatcccggcggtggtcggttgcggcaacgccacccagatcctgcaggatggccaggg

MLST allele seq: ggtgaccgtt
                 ||||||||||
Hit in genome:   ggtgaccgtt

TRP_7 is a perfect match - contig name: PA2H3_c443	allele length: 443	HSP length: 443	Gaps: 0	Percent ID: 100.00
MLST allele seq: tgtcgtgggcagctcgccggaggtgctggtacgggtcgaggatggcctggtgacggtgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgtcgtgggcagctcgccggaggtgctggtacgggtcgaggatggcctggtgacggtgcg

MLST allele seq: cccgatcgccggtacccgtccgcgcgggatcaacgaagaggccgacctggcactggagca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cccgatcgccggtacccgtccgcgcgggatcaacgaagaggccgacctggcactggagca

MLST allele seq: ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg

MLST allele seq: caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat

MLST allele seq: cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg

MLST allele seq: gctcagcgcgatggacgcgctgcgggcgattctgccggcgggcactctatccggcgcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gctcagcgcgatggacgcgctgcgggcgattctgccggcgggcactctatccggcgcgcc

MLST allele seq: gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggagtctacgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggagtctacgg

MLST allele seq: cggcgcggtcggctacctggcat
                 |||||||||||||||||||||||
Hit in genome:   cggcgcggtcggctacctggcat

*WARNING*: No perfect match for .  is the closest match - contig name: 	allele length: 	HSP length: 	Gaps: 	Percent ID:   0.00
Unknown ST	ACS_14	ARO_5	GUA_10	MUT_7	NUO_4	PPS_13	TRP_7	
