ACS_118 is a perfect match - contig name: PA2B9_c388	allele length: 390	HSP length: 390	Gaps: 0	Percent ID: 100.00
MLST allele seq: ggcccgctggccaacggcgccaccaccattctgttcgagggcgtgccgaactatcccgac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcccgctggccaacggcgccaccaccattctgttcgagggcgtgccgaactatcccgac

MLST allele seq: gtgacccgcgtggcgaagatcatcgacaagcacaaggtcaacatcctctacaccgcgccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgacccgcgtggcgaagatcatcgacaagcacaaggtcaacatcctctacaccgcgccg

MLST allele seq: accgcgatccgcgcgatgatggccgagggcaaggcggcggtggccggtgccgacggttcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   accgcgatccgcgcgatgatggccgagggcaaggcggcggtggccggtgccgacggttcc

MLST allele seq: agcctgcgtctgctcggctcggtgggcgagccgatcaacccggaagcctggcagtggtac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   agcctgcgtctgctcggctcggtgggcgagccgatcaacccggaagcctggcagtggtac

MLST allele seq: tacgagaccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tacgagaccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc

MLST allele seq: ggcgcctgcctgatgaccccgctgccgggcgcccacgcgatgaagccgggctccgcggcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcgcctgcctgatgaccccgctgccgggcgcccacgcgatgaagccgggctccgcggcc

MLST allele seq: aagccgttcttcggcgtggtaccggcgctg
                 ||||||||||||||||||||||||||||||
Hit in genome:   aagccgttcttcggcgtggtaccggcgctg

ARO_104 is a perfect match - contig name: PA2B9_c7	allele length: 498	HSP length: 498	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgtcaccgtgccgttcaaggaagaagcctatcggctggtggacgaattgagcgagcggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atgtcaccgtgccgttcaaggaagaagcctatcggctggtggacgaattgagcgagcggg

MLST allele seq: ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg

MLST allele seq: acaacaccgacggcgccggcctgctgcgggacctgacggcgaatgccggggtcgagctgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acaacaccgacggcgccggcctgctgcgggacctgacggcgaatgccggggtcgagctgc

MLST allele seq: gcggcaagcgagttctcctgctcggcgccggcggtgcggtgcgcggggtgctcgaaccct
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gcggcaagcgagttctcctgctcggcgccggcggtgcggtgcgcggggtgctcgaaccct

MLST allele seq: ttctcggcgagtgcccggcacagttgctgatcgccaaccgcacggcgcagaaggccgtgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttctcggcgagtgcccggcacagttgctgatcgccaaccgcacggcgcagaaggccgtgg

MLST allele seq: acctggccgggcagttcgccgacctcggcgcggtgcgcggctgcggtttcgccgaggtcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acctggccgggcagttcgccgacctcggcgcggtgcgcggctgcggtttcgccgaggtcg

MLST allele seq: aagggcctttcgacctggtcgtcaacggcacctcggccagccttgccggcgacgtgccgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   aagggcctttcgacctggtcgtcaacggcacctcggccagccttgccggcgacgtgccgc

MLST allele seq: cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatggca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatggca

MLST allele seq: aggaaccgaccgctttca
                 ||||||||||||||||||
Hit in genome:   aggaaccgaccgctttca

GUA_85 is a perfect match - contig name: PA2B9_c57	allele length: 373	HSP length: 373	Gaps: 0	Percent ID: 100.00
MLST allele seq: ctgctcggcctctccggtggcgtggattcctcggtggtcgccgcgctgctgcacaaggcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctgctcggcctctccggtggcgtggattcctcggtggtcgccgcgctgctgcacaaggcc

MLST allele seq: atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc

MLST allele seq: gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc

MLST allele seq: gaggacaagttcctcgggcgcctggctggcgtcgccgacccggaagagaagcgcaagatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaggacaagttcctcgggcgcctggctggcgtcgccgacccggaagagaagcgcaagatc

MLST allele seq: atcggccgcaccttcatcgaagtcttcgacgaagaagccaccaagctgcaggacgtgaag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggccgcaccttcatcgaagtcttcgacgaagaagccaccaagctgcaggacgtgaag

MLST allele seq: ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc

MLST allele seq: aaggcccacgtga
                 |||||||||||||
Hit in genome:   aaggcccacgtga

MUT_86 is a perfect match - contig name: PA2B9_c334	allele length: 442	HSP length: 442	Gaps: 0	Percent ID: 100.00
MLST allele seq: ttgcaggaagtcatcaagcgtctcgcgctggctcgcttcgacgtggctttccacttgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttgcaggaagtcatcaagcgtctcgcgctggctcgcttcgacgtggctttccacttgcgc

MLST allele seq: cataacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cataacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc

MLST allele seq: cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag

MLST allele seq: cgcaacggcctgcacctgtggggctgggtcggcttgccgaccttctcccgcagccagccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgcaacggcctgcacctgtggggctgggtcggcttgccgaccttctcccgcagccagccg

MLST allele seq: gacctacagtacttctatgtgaacgggcgcatggtacgcgacaagctggtcgcccacgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gacctacagtacttctatgtgaacgggcgcatggtacgcgacaagctggtcgcccacgcg

MLST allele seq: gtgcgccaggcttatcgcgacgtgctgtacaacggccggcacccgaccttcgtactgttc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgcgccaggcttatcgcgacgtgctgtacaacggccggcacccgaccttcgtactgttc

MLST allele seq: ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc

MLST allele seq: ttccgtgacagccggatggtcc
                 ||||||||||||||||||||||
Hit in genome:   ttccgtgacagccggatggtcc

NUO_60 is a perfect match - contig name: PA2B9_c170	allele length: 366	HSP length: 366	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg

MLST allele seq: caactggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   caactggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc

MLST allele seq: gccgagaagatggccgagcgccagtcctggcacagtttcatcccctataccgaccgcatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gccgagaagatggccgagcgccagtcctggcacagtttcatcccctataccgaccgcatc

MLST allele seq: gactacctcggcggggtgatgaacaacctgccctacgtgctctcggtggagaagctcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gactacctcggcggggtgatgaacaacctgccctacgtgctctcggtggagaagctcgcc

MLST allele seq: gggatcaaggtgccgcagcgggtcgacgtgatccggatcatgatggcggagttcttccgt
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggatcaaggtgccgcagcgggtcgacgtgatccggatcatgatggcggagttcttccgt

MLST allele seq: atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg

MLST allele seq: gtgttc
                 ||||||
Hit in genome:   gtgttc

PPS_54 is a perfect match - contig name: PA2B9_c481	allele length: 370	HSP length: 370	Gaps: 0	Percent ID: 100.00
MLST allele seq: catcgtccaggcgcgtccggaaaccgtgaagagccgcgccagcgccacggtcatggagcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   catcgtccaggcgcgtccggaaaccgtgaagagccgcgccagcgccacggtcatggagcg

MLST allele seq: ctacctgctgaaagagaaggggaccgtcctggtcgaagggcgtgccatcggccagcgcat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctacctgctgaaagagaaggggaccgtcctggtcgaagggcgtgccatcggccagcgcat

MLST allele seq: cggtgccggtccggtcaaggtgatcaacgacgtgtcggaaatggacaaggtccaaccggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cggtgccggtccggtcaaggtgatcaacgacgtgtcggaaatggacaaggtccaaccggg

MLST allele seq: tgacgtactggtctccgacatgaccgacccggactgggagccggtgatgaagcgcgccag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgacgtactggtctccgacatgaccgacccggactgggagccggtgatgaagcgcgccag

MLST allele seq: cgccatcgtcaccaaccgcggcgggcgtacctgccacgcggcgatcatcgctcgcgaact
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgccatcgtcaccaaccgcggcgggcgtacctgccacgcggcgatcatcgctcgcgaact

MLST allele seq: gggcatcccggcggtggtcggttgcggtaacgccacccagatcctgcaggatggacaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggcatcccggcggtggtcggttgcggtaacgccacccagatcctgcaggatggacaggg

MLST allele seq: ggtgaccgtt
                 ||||||||||
Hit in genome:   ggtgaccgtt

TRP_123 is a perfect match - contig name: PA2B9_c219	allele length: 443	HSP length: 443	Gaps: 0	Percent ID: 100.00
MLST allele seq: tgtagtgggcagctcgccggaggtgctggtacgggtcgaggacggcctggtcacggtgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgtagtgggcagctcgccggaggtgctggtacgggtcgaggacggcctggtcacggtgcg

MLST allele seq: cccgatcgccggtacccgaccgcgcgggatcaacgaagaggccgacctggcgctggagca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cccgatcgccggtacccgaccgcgcgggatcaacgaagaggccgacctggcgctggagca

MLST allele seq: ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg

MLST allele seq: caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat

MLST allele seq: cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg

MLST allele seq: actcagcgcgatggacgcgctgcgggcgatcctgccggcgggcacgctgtccggcgcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   actcagcgcgatggacgcgctgcgggcgatcctgccggcgggcacgctgtccggcgcgcc

MLST allele seq: gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggcgtctacgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggcgtctacgg

MLST allele seq: cggcgcggtcggctacctggcat
                 |||||||||||||||||||||||
Hit in genome:   cggcgcggtcggctacctggcat

*WARNING*: No perfect match for .  is the closest match - contig name: 	allele length: 	HSP length: 	Gaps: 	Percent ID:   0.00
Unknown ST	ACS_118	ARO_104	GUA_85	MUT_86	NUO_60	PPS_54	TRP_123	
