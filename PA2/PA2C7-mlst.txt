ACS_39 is a perfect match - contig name: PA2C7_c567	allele length: 390	HSP length: 390	Gaps: 0	Percent ID: 100.00
MLST allele seq: ggcccgctggccaacggcgccaccaccattctgttcgagggcgtgccgaactaccccgac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcccgctggccaacggcgccaccaccattctgttcgagggcgtgccgaactaccccgac

MLST allele seq: gtgacccgcgtggcgaagatcatcgacaagcacaaggtcaacatcctctacaccgcgccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgacccgcgtggcgaagatcatcgacaagcacaaggtcaacatcctctacaccgcgccg

MLST allele seq: accgcgatccgcgcgatgatggccgaaggcaaggcggcggtggccggtgccgacggttcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   accgcgatccgcgcgatgatggccgaaggcaaggcggcggtggccggtgccgacggttcc

MLST allele seq: agcctgcgtctgctcggttcggtgggcgagccgatcaacccggaagcctggcagtggtac
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   agcctgcgtctgctcggttcggtgggcgagccgatcaacccggaagcctggcagtggtac

MLST allele seq: tacgagaccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tacgagaccgtcggccagtcgcgctgcccgatcgtcgacacctggtggcagaccgagacc

MLST allele seq: ggcgcctgcctgatgaccccgctgccgggcgcccacgcgatgaagccgggctctgcagcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggcgcctgcctgatgaccccgctgccgggcgcccacgcgatgaagccgggctctgcagcc

MLST allele seq: aagccgttcttcggcgtggtaccggcactg
                 ||||||||||||||||||||||||||||||
Hit in genome:   aagccgttcttcggcgtggtaccggcactg

*WARNING*: No perfect match for ARO. ARO_5 is the closest match - contig name: PA2C7_c415	allele length: 498	HSP length: 497	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgtcaccgtgccgttcaaggaagaggcctatcgtctggtggacgagttgagcgagcggg
                  |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:    tgtcaccgtgccgttcaaggaagaggcctatcgtctggtggacgagttgagcgagcggg

MLST allele seq: ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ccacccgggccggggcggtgaacaccctgatccgcctcgccgacggtcgcctgcgcggcg

MLST allele seq: acaacaccgacggcgccggcctgctgcgggacctgacggcgaacgccggggtcgagctgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acaacaccgacggcgccggcctgctgcgggacctgacggcgaacgccggggtcgagctgc

MLST allele seq: gcggcaagcgggttctcctgctcggcgccggcggtgcggtgcgtggggtgctcgaaccct
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gcggcaagcgggttctcctgctcggcgccggcggtgcggtgcgtggggtgctcgaaccct

MLST allele seq: tcctcggcgagtgcccggcggagttgctgatcgccaaccgcacggcgcggaaggccgtgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tcctcggcgagtgcccggcggagttgctgatcgccaaccgcacggcgcggaaggccgtgg

MLST allele seq: acctggccgagcggttcgccgacctcggcgcggtgcacggctgcggtttcgccgaggtcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   acctggccgagcggttcgccgacctcggcgcggtgcacggctgcggtttcgccgaggtcg

MLST allele seq: aagggcctttcgacctgatcgtcaacggcacctcggccagtcttgccggcgacgtgccgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   aagggcctttcgacctgatcgtcaacggcacctcggccagtcttgccggcgacgtgccgc

MLST allele seq: cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatgcca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgctggcgcagagcgtgatcgagcccggccgtaccgtctgctacgacatgatgtatgcca

MLST allele seq: aggaaccgactgccttca
                 ||||||||||||||||||
Hit in genome:   aggaaccgactgccttca

GUA_9 is a perfect match - contig name: PA2C7_c298	allele length: 373	HSP length: 373	Gaps: 0	Percent ID: 100.00
MLST allele seq: ctgctcggcctctccggcggcgtggactcctcggtggtcgccgcgctgctgcacaaggcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctgctcggcctctccggcggcgtggactcctcggtggtcgccgcgctgctgcacaaggcc

MLST allele seq: atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggcgaccaactgacctgcgtgttcgtcgacaacggcctgctgcgcctgcacgaaggc

MLST allele seq: gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaccaggtgatggccatgttcgccgagaacatgggcgtgaaggtgatccgcgccaacgcc

MLST allele seq: gaggacaagttcctcggtcgcctggccggcgtcgccgacccggaagagaagcgcaagatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaggacaagttcctcggtcgcctggccggcgtcgccgacccggaagagaagcgcaagatc

MLST allele seq: atcggccgcaccttcatcgaagtcttcgacgaagaagccaccaagctgcaggacgtgaag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcggccgcaccttcatcgaagtcttcgacgaagaagccaccaagctgcaggacgtgaag

MLST allele seq: ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcctcgcccagggcaccatctaccccgacgtgatcgagtcggccggcgccaagaccggc

MLST allele seq: aaggcccacgtga
                 |||||||||||||
Hit in genome:   aaggcccacgtga

MUT_11 is a perfect match - contig name: PA2C7_c125	allele length: 442	HSP length: 442	Gaps: 0	Percent ID: 100.00
MLST allele seq: ctgcaggaagtcatcaagcgcctggcgctggcccgtttcgacgtggctttccacctgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctgcaggaagtcatcaagcgcctggcgctggcccgtttcgacgtggctttccacctgcgc

MLST allele seq: cacaacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cacaacggcaagaccatcttcgccctgcacgaggcgcgagacgagctggcccgcgcgcgc

MLST allele seq: cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgggtcggcgcggtgtgcggccaggcattcctcgagcaggcgctgccgatcgaggtcgag

MLST allele seq: cgcaacggcctgcacctgtggggttgggtcggcttgccgaccttctcccgcagccagccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgcaacggcctgcacctgtggggttgggtcggcttgccgaccttctcccgcagccagccg

MLST allele seq: gacctgcagtacttctatgtgaacgggcgcatggtgcgcgacaagctggtcgcccacgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gacctgcagtacttctatgtgaacgggcgcatggtgcgcgacaagctggtcgcccacgcg

MLST allele seq: gtgcgccaggcttatcgcgacgtgctgtacaacggccggcatccgaccttcgtgctgttc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gtgcgccaggcttatcgcgacgtgctgtacaacggccggcatccgaccttcgtgctgttc

MLST allele seq: ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ttcgaagtcgatccggcggtggtggacgtcaacgtgcacccgaccaagcacgaagttcgc

MLST allele seq: ttccgtgacagccggatggtcc
                 ||||||||||||||||||||||
Hit in genome:   ttccgtgacagccggatggtcc

NUO_27 is a perfect match - contig name: PA2C7_c305	allele length: 366	HSP length: 366	Gaps: 0	Percent ID: 100.00
MLST allele seq: atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atgttcctcaacctcggcccgaaccacccgtccgcccacggcgcgttccgcatcatcctg

MLST allele seq: cagctggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cagctggacggcgaggagatcatcgactgcgtcccggagatcggctaccaccaccgcggc

MLST allele seq: gccgagaagatggccgagcgccagtcctggcacagtttcattccctacaccgaccgcatc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gccgagaagatggccgagcgccagtcctggcacagtttcattccctacaccgaccgcatc

MLST allele seq: gactacctcggcggggtgatgaacaacctgccctacgtactctcggtggagaagctcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gactacctcggcggggtgatgaacaacctgccctacgtactctcggtggagaagctcgcc

MLST allele seq: gggatcaaggtgccccagcgggtcgacgtgatccggatcatgatggcggagttcttccgt
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggatcaaggtgccccagcgggtcgacgtgatccggatcatgatggcggagttcttccgt

MLST allele seq: atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   atcctgaaccacctgctgtacctgggcacctatatccaggacgtcggcgccatgaccccg

MLST allele seq: gtgttc
                 ||||||
Hit in genome:   gtgttc

PPS_4 is a perfect match - contig name: PA2C7_c66	allele length: 370	HSP length: 370	Gaps: 0	Percent ID: 100.00
MLST allele seq: catcgtccaggcacgcccggaaaccgtgaagagccgcgccagcgccacggtcatggagcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   catcgtccaggcacgcccggaaaccgtgaagagccgcgccagcgccacggtcatggagcg

MLST allele seq: ctacctgctgaaagagaaggggaccgtcctggtggaagggcgtgccatcggccagcgcat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ctacctgctgaaagagaaggggaccgtcctggtggaagggcgtgccatcggccagcgcat

MLST allele seq: cggtgccggtccggtcaaggtgatcaacgacgtgtcggaaatggacaaggtccaaccggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cggtgccggtccggtcaaggtgatcaacgacgtgtcggaaatggacaaggtccaaccggg

MLST allele seq: tgacgtcctggtctccgacatgaccgacccggactgggagccggtgatgaagcgcgccag
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgacgtcctggtctccgacatgaccgacccggactgggagccggtgatgaagcgcgccag

MLST allele seq: cgccatcgtcaccaaccgcggcgggcgtacctgccacgcggcgatcatcgctcgcgaact
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgccatcgtcaccaaccgcggcgggcgtacctgccacgcggcgatcatcgctcgcgaact

MLST allele seq: gggcatcccggcggtggtcggttgcggcaacgccacccagatcctgcaggatggccaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gggcatcccggcggtggtcggttgcggcaacgccacccagatcctgcaggatggccaggg

MLST allele seq: ggtgaccgtt
                 ||||||||||
Hit in genome:   ggtgaccgtt

TRP_2 is a perfect match - contig name: PA2C7_c91	allele length: 443	HSP length: 443	Gaps: 0	Percent ID: 100.00
MLST allele seq: tgtcgtgggcagctcgccggaggtgctggtacgggtcgaggatggcctggtgacggtgcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   tgtcgtgggcagctcgccggaggtgctggtacgggtcgaggatggcctggtgacggtgcg

MLST allele seq: cccgatcgccggtacccgtccgcgcgggatcaacgaagaggccgacctggcactggagca
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cccgatcgccggtacccgtccgcgcgggatcaacgaagaggccgacctggcactggagca

MLST allele seq: ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   ggatctgctgtcggacgccaaggagatcgccgagcacctgatgctgatcgacctggggcg

MLST allele seq: caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   caacgacgtggggcgggtgtccgatatcggcgcggtgaaggtcaccgaaaaaatggtgat

MLST allele seq: cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   cgaacgttactccaacgtcatgcacatcgtgtccaacgtcaccgggcaattgcgcgaggg

MLST allele seq: gctcagcgcgatggacgcgctgcgggcgatcctgccggcgggcactctatccggcgcgcc
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gctcagcgcgatggacgcgctgcgggcgatcctgccggcgggcactctatccggcgcgcc

MLST allele seq: gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggagtctacgg
                 ||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
Hit in genome:   gaagatccgcgccatggagatcatcgacgagctggagccggtcaagcgtggagtctacgg

MLST allele seq: cggcgcggtcggctacctggcat
                 |||||||||||||||||||||||
Hit in genome:   cggcgcggtcggctacctggcat

*WARNING*: No perfect match for .  is the closest match - contig name: 	allele length: 	HSP length: 	Gaps: 	Percent ID:   0.00
Unknown ST	ACS_39	ARO_5	GUA_9	MUT_11	NUO_27	PPS_4	TRP_2	
Please note that one or more of the MLST alleles exceed the length of the contig containing the MLST locus. See the extended output for more information
