

from fasta_mlst_tocsv import get_fasta_list, make_allele_table

def test_muudet():

    fasta = get_fasta_list('muudet_kokku.fasta')

    output_filename = 'test_output.csv'
    make_allele_table(fasta, output_filename)

    with open(output_filename, 'r') as f:
        lines = f.readlines()
    
    for l in lines:
        if l.startswith('isolate'):
            continue
        if l.startswith('EC_A5'):
            assert l.split(',')[1] == '83'
            for title, sequence in fasta:
                if title.find('EC_A5') > 0:
                    assert l.split(',')[-1].find(sequence) != -1
