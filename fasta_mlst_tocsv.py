from collections import defaultdict
import argparse
import pandas as pd
from Bio.SeqIO.FastaIO import SimpleFastaParser

def make_parser():
    desc = 'Make MLST allele table from FASTA file.'
    parser = argparse.ArgumentParser(description=desc)
    parser.add_argument(metavar='S',
                        dest='filename', default='default.fasta',
                        help='FASTA file used as input for this script.')
    return parser

#creates an output filename out of the given input filename
def make_output_filename(input_filename):
    return input_filename.split('.')[0] + '.csv'

#creates a list of tuples with the title and sequence in a fasta file; example: [(>title, sequence)]
def get_fasta_list(input_filename):
    fasta = []
    with open(input_filename) as fasta_file:  # Will close handle cleanly
        for title, sequence in SimpleFastaParser(fasta_file):
            fasta.append((title, sequence))

    return fasta

#uses the output of get_fasta_list function and creates a csv file with MLST allele table.
def make_allele_table(fasta, filename):

    sorted_fasta = sorted(fasta, key=lambda gene:gene[0]) #sorts the fasta file, so that the isolates and genes would be
                                                          #in the right (as in MLST sceme) and same order

    ###Starts go through the file and puts them in dictionary my_data, which is a dictionary of tuples
    isolates = []
    my_data = defaultdict(lambda:defaultdict(tuple))
    sequences = defaultdict(str)

    for title, sequence in sorted_fasta:
        title_els = {} 
        for index, key in enumerate(['ID', 'gene_name', 'allele', 'percent']):
            title_els[key] = title.split('|')[index].split(':')[1] 

        isolates.append(title_els['ID']) 

        sequences[title_els['ID']] += sequence
        #print title_els['percent']
        if title_els['percent'] == 'SeqLengthError':
            title_els['allele'] += ' SeqError'

        elif float(title_els['percent']) < 100.00:
            title_els['allele'] += '*'
        else:
            title_els['allele']
            
        info = (title_els['allele'], sequence)

        my_data[title_els['ID']][title_els['gene_name']] = info

#goes through the previously created dictionary and creates a nested list out of it to make a DataFrame.
    bacteria_isolates = []
    ##Change the names of gene alleles in gene_keys if different organism##
    gene_keys = ['ACS', 'ARO', 'GUA', 'MUT', 'NUO', 'PPS', 'TRP']
    columns = ['isolate'] + gene_keys + ['sequence']

    for isolates, d in my_data.iteritems():

        gene_list = []
        seq = str()
        for key in gene_keys:
            #print key
            gene_list.append(d[key][0])
            seq += d[key][1]
        bacteria_isolates.append([isolates] + gene_list + [seq])

    df = pd.DataFrame(bacteria_isolates, columns=columns)

    df.to_csv(filename, index=False)
    print 'Tegi uue faili nimega: {0}'.format(filename) 
    
if __name__ == '__main__':
    
    parser = make_parser()
    args = parser.parse_args()
    fasta = get_fasta_list(args.filename)
    output_filename = make_output_filename(args.filename)
    make_allele_table(fasta, output_filename)
