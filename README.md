# Use allele_table_from_txt.py - most up to date and creates .fasta file and .csv table as well

## What does script "allele_table_from_txt.py" do?
This scirpt creates an MLST allele table from the  MLST sequence information txt file.
The txt file has to look like this:

```
ADK-6 is a perfect match: EC_A1_c24 length: 536 
MLST allele seq: ggggaaagggactcaggctcagttcatcat 
                 ||||||||||||||||||||||||||||||
Hit in genome:   ggggaaagggactcaggctcagttcatcat

MLST allele seq: tggcgatatgctgcgtgctgcggtcaaatc
                 ||||||||||||||||||||||||||||||
Hit in genome:   tggcgatatgctgcgtgctgcggtcaaatc

MLST allele seq: cattatggatgctggcaaactggtcaccga
                 ||||||||||||||||||||||||||||||
Hit in genome:   cattatggatgctggcaaactggtcaccga
```

##Most common error: tuple index out of range
Usually this comes when the program tries to make an CSV table form the FASTA file. And mostly, so far, this is because the every isolate/contig does not have the same number of alleles. One of them as less or more.


### Usage  
**python allele_table_from_txt.py -f file1 file2 -o output.fasta** (output file can be leaved unnamed, the default output filename is output.fasta)

### Warning
If you already have an output.fasta in your directory and do not give a name to the output file then the previous filename will be overwrittern. It will not give an error message or any info for this, just so you know.
### Output
Creates a csv formated MLST allele table from MLST fasta file.
The output csv file will contain the name of the isolate with the allele type for each allele. A '*' flag after the number means that this is a unique ST and the number refers to the closest match in the database. Flag 'SeqError' means that the sequence for this allele was shorter than should have been and the concatenated sequence is also wrong.

### Help
If you type in the terminal **python allele_table_from_txt.py -h** it will give a help output that shows the example input format and the keys for inputfiles, outputfiles. If you have any questions, come find me or write <vvoolaid@gmail.com>. 

## Older versions
The previous versions were as two separate scripts, but are still functional and usable.
### create_fasta.py
This creates a fasta file that has the MLST allele information extracted from the input txt file. The example file is shown above. It takes in several txt files and creates a single fasta file. The heading will contain Isolate name, gene name, allele, similarity % with the closest match.
#### Usage  
**python create_fasta.py -f file1 file2 -o output.fasta** (output file can be leaved unnamed, the default output filename is output.fasta)

#### Warning
If you already have an output.fasta in your directory and do not give a name to the output file then the previous filename will be overwrittern. It will not give an error message or any info for this, just so you know.

### fasta_mlst_tocsv.py
Creates a csv formated MLST allele table from MLST fasta file. The input fasta file needs to be in a format as the output from create_fasta.py. If the heading of the fasta file is different then the results in the table will most probably be wrong.
The output csv file will contain the name of the isolate with the allele type for each allele. A '*'flag after the number means that this is a unique ST and the number refers to the closest match in the database. Flag 'SeqError' means that the sequence for this allele was shorter than should have been and the concatenated sequence is also wrong.
#### Usage
**fasta_mlst_tocsv.py input.fasta**

## Problems if different species used
If the .txt files have been generated for different species then it's good to check the **'els'-object**. If there are any differnces then the isolate names and contig_no will be incorrect, just some gibberish.
Another thing to check is **gene_list**. These names are different for different organisms and if they do not match in the object with the ones in the file then it will give an error. 
	
	Traceback (most recent call last):
	  File "/home/lubuntu/git/bitbucket/mlst/fasta_mlst_tocsv.py", line 80, in <module>
	    make_allele_table(fasta, output_filename)
	  File "/home/lubuntu/git/bitbucket/mlst/fasta_mlst_tocsv.py", line 65, in make_allele_table
	    gene_list.append(d[key][0])
	IndexError: tuple index out of range
