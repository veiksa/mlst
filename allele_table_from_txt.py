from collections import defaultdict
import argparse
import fileinput
import textwrap
import pandas as pd
from Bio.SeqIO.FastaIO import SimpleFastaParser


def make_parser():
    desc = textwrap.dedent('''Make a fasta file from:

ADK-6 is a perfect match: EC_A1_c24 length: 536 
MLST allele seq: ggggaaagggactcaggctcagttcatcat 
                 ||||||||||||||||||||||||||||||
Hit in genome:   ggggaaagggactcaggctcagttcatcat

MLST allele seq: tggcgatatgctgcgtgctgcggtcaaatc
                 ||||||||||||||||||||||||||||||
Hit in genome:   tggcgatatgctgcgtgctgcggtcaaatc

MLST allele seq: cattatggatgctggcaaactggtcaccga
                 ||||||||||||||||||||||||||||||
Hit in genome:   cattatggatgctggcaaactggtcaccga

 And create a table in CSV from the MLST allele numbers.
 python 'python_script.py' -f file1.txt file2.txt -o (optional default=output.fasta)
  CSV file will be with the same name as output''')
    
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=desc)
    parser.add_argument('-f', '--filenames', metavar='S', nargs='+',
                        dest='filenames', default='filename.fasta',
                        help='TXT file names, as in the example, used as input for this script.')
    parser.add_argument('-o', '--output_filename', metavar='S',
                         dest='output_name', default='output.fasta',
                         help='The default output filename is output.fasta, but user can name the output.')
    return parser

#reads in n number of files and makes a list object with the content of the file
def read_in_files(files):
    input_lines_together = []
    inputs = fileinput.FileInput(files=(files))
    for line in inputs:
        input_lines_together.append(line)
    return input_lines_together

#makes a fasta file from the example type txt file. The heading is determined by program    
def create_fasta(inputfile, output_filename):
    seqdict = defaultdict(str)
    for line_nr, x in enumerate(inputfile):
        #print x
        if x.strip() == '' or x.startswith('MLST allele seq') or x.strip().startswith('|'):
            continue
        if x.startswith('Hit'):
            gene_str = x.split(':')[1].strip()
            #print gene_str
            if gene_str != '':
                #print (header, gene_str)
                seqdict[header] += gene_str
            continue
        header = x.strip() + ', line_nr: {0}'.format(line_nr)
    #print seqdict
    print 'Creating: {}'.format(output_filename)
    with open(output_filename, 'w') as f:
        for k, v in seqdict.items():
            els = k.replace(' ', '').split()
            #print els
            header_info = els[0].replace('_', '-',1).split('-') ##This changes the header info since this can be changed in the files if organism is different, check!##

            #checks if string 'rep' is in isolate name and fixes if needed
            if header_info[-1].split(':')[-1].rsplit('_',1)[0].find('rep') > 0:
                isolate = header_info[-1].split(':')[-1].rsplit('_',2)[0]
            else:
                isolate = header_info[-1].split(':')[-1].rsplit('_',1)[0]

            gene = header_info[0].split('.')[-1]
            allel_no = header_info[1].split('i')[0]
            percentID = els[4].split(',')[0]

            #looks if the Hit in Genome is a long as MLST database gene
            #if not, puts a flag to the csv table and also to the fasta percentID
            if len(v) == int(els[1].split(':')[1]):
                percentID = els[4].split(',')[0]
            else:
                percentID = 'PercentID:SeqLengthError:LengthInGenome-' + str(len(v)) + ':LengthShould-' + els[1].split(':')[1]
                print 'There is something wrong with isolate ' + isolate + ' gene ' +  gene + ' length; it has been flagged in the FASTA file and in the table'

            new_k = '|'.join(['isolate:' + isolate , 'gene:' + gene ,
                              'allel_no:' +allel_no, percentID])
                                               #paneb fasta headeri kasutajale
                                               #sobivamasse j2rjekorda
            f.write('>{0}\n{1}\n'.format(new_k, v)) #faili kirjutamine
                                                    #string formating; tuleb lugeda


#creates a list of tuples with the title and sequence in a fasta file; example: [(>title, sequence)]
def get_fasta_list(input_filename):
    fasta = []
    with open(input_filename) as fasta_file:  # Will close handle cleanly
        for title, sequence in SimpleFastaParser(fasta_file):
            fasta.append((title, sequence))

    return fasta

#creates an output filename out of the given input filename
def make_output_filename(input_filename):
    return input_filename.split('.')[0] + '.csv'


#uses the output of get_fasta_list function and creates a csv file with MLST allele table.
def make_allele_table(fasta, filename):

    sorted_fasta = sorted(fasta, key=lambda gene:gene[0]) #sorts the fasta file, so that the isolates and genes would be
                                                          #in the right (as in MLST sceme) and same order
    #print sorted_fasta
    ###Starts go through the file and puts them in dictionary my_data, which is a dictionary of tuples
    isolates = []
    my_data = defaultdict(lambda:defaultdict(tuple))
    sequences = defaultdict(str)

    for title, sequence in sorted_fasta:
        title_els = {} 
        for index, key in enumerate(['ID', 'gene_name', 'allele', 'percent']):
            title_els[key] = title.split('|')[index].split(':')[1] 

        isolates.append(title_els['ID']) 

        sequences[title_els['ID']] += sequence

        if title_els['percent'] == 'SeqLengthError':
            title_els['allele'] += ' SeqError'

        elif float(title_els['percent']) < 100.00:
            title_els['allele'] += '*'
        else:
            title_els['allele']
            
        info = (title_els['allele'], sequence)

        my_data[title_els['ID']][title_els['gene_name']] = info

#goes through the previously created dictionary and creates a nested list out of it to make a DataFrame.
    bacteria_isolates = []
##Change the names of gene alleles in gene_keys if different organism##
    gene_keys = ['ACS', 'ARO', 'GUA', 'MUT', 'NUO', 'PPS', 'TRP']
    columns = ['isolate'] + gene_keys + ['sequence']

    for isolates, d in my_data.iteritems():
        gene_list = []
        seq = str()
        for key in gene_keys:
            #print d[key][0]
            #print d
            gene_list.append(d[key][0])
            seq += d[key][1]
        bacteria_isolates.append([isolates] + gene_list + [seq])

    df = pd.DataFrame(bacteria_isolates, columns=columns)

    df.to_csv(filename, index=False)
    print 'Tegi uue faili nimega: {0}'.format(filename) 

        
if __name__ == '__main__':
    parser = make_parser()
    args = parser.parse_args()
    read_in_files = read_in_files(args.filenames)
    output_fasta = create_fasta(read_in_files, args.output_name)
    fasta = get_fasta_list(args.output_name)
    output_filename = make_output_filename(args.output_name)
    make_allele_table(fasta, output_filename)
