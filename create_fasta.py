from collections import defaultdict
import argparse
import fileinput
import textwrap

def make_parser():
    desc = textwrap.dedent('''Make a fasta file from:

ADK-6 is a perfect match: EC_A1_c24 length: 536 
MLST allele seq: ggggaaagggactcaggctcagttcatcat 
                 ||||||||||||||||||||||||||||||
Hit in genome:   ggggaaagggactcaggctcagttcatcat

MLST allele seq: tggcgatatgctgcgtgctgcggtcaaatc
                 ||||||||||||||||||||||||||||||
Hit in genome:   tggcgatatgctgcgtgctgcggtcaaatc

MLST allele seq: cattatggatgctggcaaactggtcaccga
                 ||||||||||||||||||||||||||||||
Hit in genome:   cattatggatgctggcaaactggtcaccga''')
    
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter, description=desc)
    parser.add_argument('-f', '--filenames', metavar='S', nargs='+',
                        dest='filenames', default='default.txt',
                        help='TXT file names, as in the example, used as input for this script.')
    parser.add_argument('-o', '--output_filename', metavar='S',
                         dest='output_name', default='output.fasta',
                         help='The default output filename is output.fasta, but user can name the output.')
    return parser


def read_in_files(files):
    input_lines_together = []
    inputs = fileinput.FileInput(files=(files))
    for line in inputs:
        input_lines_together.append(line)
    return input_lines_together
# fn = 'muudet_kokku.txt'
# with open(fn,'r') as f:
#     file_sisse = f.readlines()
# #print file_sisse
def create_fasta(inputfile, output_filename):
    seqdict = defaultdict(str)
    for line_nr, x in enumerate(inputfile):
        #print x
        if x.strip() == '' or x.startswith('MLST allele seq') or x.strip().startswith('|'):
            continue
        if x.startswith('Hit'):
            gene_str = x.split(':')[1].strip()
            #print gene_str
            if gene_str != '':
                #print (header, gene_str)
                seqdict[header] += gene_str
            continue
        header = x.strip() + ', line_nr: {0}'.format(line_nr)
    #print seqdict
    print 'Creating: {}'.format(output_filename)
    with open(output_filename, 'w') as f:
        for k, v in seqdict.items():
            
            els = k.replace(' ', '').split()
            header_info = els[0].replace('_', '-',1).split('-') ##This changes the header info since this can be changed in the files if organism is different, check!##
            ##print 'header info ', header_info
            #checks if string 'rep' (which means replication/repeat) is in isolate name and fixes if needed
            if header_info[-1].split(':')[-1].rsplit('_',1)[0].find('rep') > 0:
                isolate = header_info[-1].split(':')[-1].rsplit('_',2)[0]
                #print 'isolat with ', isolate
            else:
                isolate = header_info[-1].split(':')[-1].rsplit('_',1)[0]
                #print 'isolate without rep ', isolate

            gene = header_info[0].split('.')[-1]
            #print 'gene ', gene
            allel_no = header_info[1].split('i')[0]
            #print 'allel_no ', allel_no

            #looks if the Hit in Genome is as long as MLST database gene
            #if not, puts a flag to the csv table and also to the fasta percentID
            if len(v) == int(els[1].split(':')[1]):
                percentID = els[4].split(',')[0]
            else:
                percentID = 'PercentID:SeqLengthError:LengthInGenome-' + str(len(v)) + ':LengthShould-' + els[1].split(':')[1]
                print 'There is something wrong with isolate ' + isolate + ' gene ' +  gene + ' length; it has been flagged in the FASTA file and in the table'
                
            new_k = '|'.join(['isolate:' + isolate , 'gene:' + gene ,
                    'allel_no:' +allel_no, percentID])
                                               #paneb fasta headeri kasutajale
                                               #sobivamasse j2rjekorda
            f.write('>{0}\n{1}\n'.format(new_k, v)) #faili kirjutamine
                                                    #string formating; tuleb lugeda


        
if __name__ == '__main__':
    parser = make_parser()
    args = parser.parse_args()
    read_in_files = read_in_files(args.filenames)
    output_fasta = create_fasta(read_in_files, args.output_name)
